# Aide et ressources de i2PEPICO pour Synchrotron SOLEIL

## Résumé

- conversion puis extraction des vitesses des ions et des électrons
- Créé à Synchrotron Soleil

## Sources

- Code source: Oui, mais sur un dépôt interne a Soleil
- Documentation officielle: [Mode d'emploi en pdf sur gitlab](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/i2pepico-documentation/-/blob/master/documents/ModeEmploiei2PEPICO.pdf?expanded=true&viewer=rich)

## Navigation rapide


| Fichiers téléchargeables |
| - |
| [Documents d'utilisation](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/i2pepico-documentation/-/tree/master/documents) | 

## Installation

- Systèmes d'exploitation supportés: Windows, MacOS
- Installation: Facile (tout se passe bien), installation igor nécessaire

## Format de données

- en entrée: binaire, séquence de mot 32 bits
- en sortie: fichier binaire, structure en c, fichiers texte, fichiers image, fichier igor
- sur un disque dur, sur la Ruche locale, git interne à SOLEIL
